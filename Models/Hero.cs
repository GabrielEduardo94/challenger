using System;
using System.Collections.Generic;

namespace ChallangerEY.Models
{
    public class Hero
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public DateTime DataCadastro { get; set; }
        public bool Ativo { get; set; }
        
        
        public virtual IEnumerable<HeroPower> HeroisPoderes { get; set; }
        public virtual Universe Universo { get; set; }
        
    }
}