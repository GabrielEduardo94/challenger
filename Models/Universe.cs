using System;
using System.Collections.Generic;

namespace ChallangerEY.Models
{
    public class Universe
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        
        public virtual IEnumerable<Hero> Herois { get; set; }

    }
}