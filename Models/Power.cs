using System;
using System.Collections.Generic;

namespace ChallangerEY.Models
{
    public class Power
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        
        public virtual IEnumerable<HeroPower> HeroisPoderes { get; set; }

    }
}