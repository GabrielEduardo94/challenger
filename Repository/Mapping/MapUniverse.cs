using ChallangerEY.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ChallangerEY.Repository.Mapping
{
    public class MapUniverse: IEntityTypeConfiguration<Universe>
    {
        public void Configure(EntityTypeBuilder<Universe> builder)
        {
            builder
                .ToTable("tbUniverse");

            builder
                .Property(p => p.Id)
                .HasColumnName("Id")
                .IsRequired();

            builder
                .HasKey(k => k.Id)
                .HasName("pkUniverse");

            builder
                .Property(p => p.Nome)
                .HasColumnName("Nome")
                .HasColumnType("varchar(80)")
                .HasMaxLength(80)
                .IsRequired();
        }


    }
}