using ChallangerEY.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ChallangerEY.Repository.Mapping
{
    public class MapHeroPower : IEntityTypeConfiguration<HeroPower>
    {
        public void Configure(EntityTypeBuilder<HeroPower> builder)
        {
            builder
                .ToTable("tbHeroPower");

            builder
                .HasKey(k => new { k.IdHero, k.IdPower })
                .HasName("pkHeroPower");

            builder
                .HasOne(q => q.Hero)
                .WithMany(q => q.HeroisPoderes)
                .HasForeignKey(k => k.IdHero)
                .HasConstraintName("fkHeroPowerHeroi")
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasIndex(k => k.IdHero)
                .HasName("ixHeroPowerIdHeroi");

            builder
                .HasOne(q => q.Power)
                .WithMany(q => q.HeroisPoderes)
                .HasForeignKey(k => k.IdPower)
                .HasConstraintName("fkHeroPowerPower")
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasIndex(p => p.IdPower)
                .HasName("ixHeroPowerIdPower");

        }
    }
}