using ChallangerEY.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ChallangerEY.Repository.Mapping
{
    public class MapPower: IEntityTypeConfiguration<Power>
    {
        public void Configure(EntityTypeBuilder<Power> builder)
        {
            builder
                .ToTable("tbPower");

            builder
                .Property(p => p.Id)
                .HasColumnName("Id")
                .IsRequired();

            builder
                .HasKey(k => k.Id)
                .HasName("pkPower");

            builder
                .Property(p => p.Nome)
                .HasColumnName("Nome")
                .HasColumnType("varchar(80)")
                .HasMaxLength(80)
                .IsRequired();


        }
    }
}