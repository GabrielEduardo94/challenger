using ChallangerEY.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ChallangerEY.Repository.Mapping
{
    public class MapHero : IEntityTypeConfiguration<Hero>
    {
        public void Configure(EntityTypeBuilder<Hero> builder)
        {
            builder
                .ToTable("tbHero");

            builder
                .Property(p => p.Id)
                .HasColumnName("Id")
                .IsRequired();

            builder
                .HasKey(k => k.Id)
                .HasName("pkHero");

            builder
                .Property(p => p.Nome)
                .HasColumnName("Nome")
                .HasColumnType("varchar(80)")
                .HasMaxLength(80)
                .IsRequired();
            
            builder
                .Property(p => p.Ativo)
                .HasColumnName("Ativo")
                .IsRequired();
            
            builder
                .Property(p => p.DataCadastro)
                .HasColumnName("DataCadastro")
                .IsRequired();
            
            builder
                .HasOne(a => a.Universo)
                .WithMany(q => q.Herois)
                .HasForeignKey(k => k.Id)
                .HasConstraintName("fkUniversoHeroi")
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasIndex(k => k.Id)
                .HasName("ixHeroiCodigoUniverso");

            

        }
    }
}