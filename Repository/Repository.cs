using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ChallangerEY.Repository
{
    public abstract class Repository<T> where T : class
    {
        protected readonly HeroContext.HeroContext Db;
        protected readonly DbSet<T> DbSet;

        public Repository(HeroContext.HeroContext context)
        {
            Db = context;
            DbSet = Db.Set<T>();
        }

        #region "EF"

        public virtual void Adicionar(T entity)
        {
            DbSet.Add(entity);
        }

        public virtual void Adicionar(IEnumerable<T> entities)
        {
            DbSet.AddRange(entities.ToArray());
        }

        public virtual void Atualizar(T entity)
        {
            DbSet.Update(entity);
        }

        public virtual void Atualizar(IEnumerable<T> entities)
        {
            DbSet.UpdateRange(entities.ToList());
        }


        public virtual void Remover(Guid codigo)
        {
            var entity = DbSet.Find(codigo);

            if (entity != null)
                DbSet.Remove(entity);
        }

        public int Salvar()
        {
            return Db.SaveChanges();
        }

        #endregion
    }
}