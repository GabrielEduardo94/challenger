using System.IO;
using ChallangerEY.Models;
using ChallangerEY.Repository.Mapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ChallangerEY.Repository.HeroContext
{
    public class HeroContext : DbContext
    {
        public HeroContext(){}
        
        public HeroContext(DbContextOptions<HeroContext> options) : base(options)
        {}
        
        public DbSet<Hero> Herois { get; set; }
        public DbSet<Power> Poderes { get; set; }
        public DbSet<Universe> Universos { get; set; }
        public DbSet<HeroPower> HeroisPoderes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MapHero());
            modelBuilder.ApplyConfiguration(new MapPower());
            modelBuilder.ApplyConfiguration(new MapUniverse());
            modelBuilder.ApplyConfiguration(new MapHeroPower());
            
            base.OnModelCreating(modelBuilder);
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));
            optionsBuilder.EnableSensitiveDataLogging();
        }

        


    }
}