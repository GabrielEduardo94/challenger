using ChallangerEY.Models;

namespace ChallangerEY.Repository
{
    public class HeroRepository : Repository<Hero>
    {
        public HeroRepository(HeroContext.HeroContext context) : base(context)
        {
        }
        
    }
}