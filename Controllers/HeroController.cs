using ChallangerEY.Models;
using ChallangerEY.Repository;
using ChallangerEY.Repository.HeroContext;
using Microsoft.AspNetCore.Mvc;

namespace ChallangerEY.Controllers
{
    public class HeroController : Controller
    {

        private readonly HeroRepository repo;
        
        public HeroController(HeroRepository repository)
        {
            repo = repository;
        }

        [HttpPost]
        public bool CadastrarHero(Hero entity)
        {

            using (var context = new HeroContext())
            {
                using (var transacao = context.Database.BeginTransaction())
                {
                    repo.Adicionar(entity);

                    repo.Salvar();
                    
                    transacao.Commit();
                }
            }
            
            return true;
        }
        
        [HttpGet]
        public bool ListarHero()
        {

            using (var context = new HeroContext())
            {
                using (var transacao = context.Database.BeginTransaction())
                {
                    
                    
                    transacao.Commit();
                }
            }
            
            return true;
        }
    }
}